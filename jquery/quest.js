//  AJOUT DE CSS SUR LES DIV DES QUESTIONS
var x = $('.question')
$(document).ready(function() {
  x.css('background', '#44B7C9');
  x.css('border','3px solid grey');
  x.css('margin-bottom','0.5cm');
  $('img').css('margin-left', '30cm');
  $('img').css('float', 'top');
});


// LE BOUTON "TESTER LES REPONSES"
$(document).ready(function() {
  $('a').css('margin-bottom', '0.5cm');
});

// CACHER LES REPONSES
$(document).ready(function() {
  $('.reponse').hide();
});

// CONDITONS REPONSES
$(document).ready(function() {
    $('a').hover(function() {
      $('.reponse').show();
      if ($(':radio[id="r1"]:checked').val()) {
        $('#reponse1').css('color','#4CAF50');
        $('#img1').attr('src', 'image/checked.png');
      }
      else {
        $('#reponse1').css('color','#FF0000');
        $('#img1').attr('src','image/cross.png');
      }
      if ($(':radio[id="r2"]:checked').val()) {
        $('#reponse2').css('color','#4CAF50');
        $('#img2').attr('src', 'image/checked.png');
      }
      else {
        $('#reponse2').css('color','#FF0000');
        $('#img2').attr('src','image/cross.png');
      }
      if ($(':radio[id="r3"]:checked').val()) {
        $('#reponse3').css('color','#4CAF50');
        $('#img3').attr('src', 'image/checked.png');
      }
      else {
        $('#reponse3').css('color','#FF0000');
        $('#img3').attr('src','image/cross.png');
      }
        $('a').mouseleave(function() {
          $('.reponse').hide();
          $('img').attr('src', 'image/help.png');
        });
    });
});
